#!/usr/bin/python3

import pyperclip
from openpyxl import load_workbook
import csv
import datetime
import yaml

clipData = pyperclip.paste()
print(clipData)

configFile = open('config.yaml', 'r')
config = yaml.load(configFile, Loader=yaml.FullLoader)
print(config)

currentDate = datetime.date.today()
dayDifference = datetime.timedelta(days = -6)
startDate = currentDate + dayDifference
startDateStr = startDate.strftime('%d')
if startDate.month != currentDate.month:
  startDateStr = startDate.strftime('%d %B')
endDateStr = currentDate.strftime('%d %B %Y')

destinationFilename = 'Weekly_Report_XC_' + currentDate.strftime('%Y%m%d') + '.xlsx'
workbook = load_workbook(filename = 'Weekly_Report_Template.xlsx')
worksheet = workbook.active
worksheet.title = startDateStr + '-' + endDateStr

taskOffset = config['task']['rowStart']
taskList = list(csv.reader(clipData.splitlines(), delimiter='\t'))
taskList.sort(key=lambda s: (s[0] + s[1]).casefold())

for row in taskList:
  worksheet[config['task']['column']['category'] + str(taskOffset)] = row[0]
  worksheet[config['task']['column']['subject'] + str(taskOffset)] = row[1]
  worksheet[config['task']['column']['hour'] + str(taskOffset)] = row[3]
  taskOffset += 1

categoryDict = {}
for row in taskList:
  if(categoryDict.get(row[0])):
    categoryDict[row[0]] += float(row[3])
  else:
    categoryDict[row[0]] = float(row[3])

categoryOffset = config['summary']['rowStart']
totalHours = 0.00
totalDays = 0.00
for category, hours in categoryDict.items():
  day = float(hours/7.00)
  worksheet[config['summary']['column']['category'] + str(categoryOffset)] = category
  worksheet[config['summary']['column']['totalHours'] + str(categoryOffset)] = hours
  worksheet[config['summary']['column']['totalDays'] + str(categoryOffset)] = day
  categoryOffset += 1
  totalHours += hours
  totalDays += day

worksheet[config['grandTotalHours']] = totalHours
worksheet[config['grandTotalDays']] = totalDays

workbook.save(filename = destinationFilename)